function afficherJour(day){
	switch(day){
		case 0:
			jourSemaine = "Lun";
			fetch_ended(0, true);
			//alert("Lundi");
			document.getElementById("boutonJourLundi").style.background= "#4C6CA0 ";
			document.getElementById("boutonJourMardi").style.background= "#75A3E7 ";
			document.getElementById("boutonJourMercredi").style.background= "#75A3E7 ";
			document.getElementById("boutonJourJeudi").style.background= "#75A3E7 ";
			document.getElementById("boutonJourVendredi").style.background= "#75A3E7 ";
			break;
		case 1:
			jourSemaine = "Mar";
			fetch_ended(1, true);
			//alert("Mardi");
			document.getElementById("boutonJourLundi").style.background= "#75A3E7 ";
			document.getElementById("boutonJourMardi").style.background= "#4C6CA0 ";
			document.getElementById("boutonJourMercredi").style.background= "#75A3E7 ";
        	document.getElementById("boutonJourJeudi").style.background= "#75A3E7 ";
			document.getElementById("boutonJourVendredi").style.background= "#75A3E7 ";
			break;
		case 2:
			jourSemaine = "Mer";
			fetch_ended(2, true);
			//alert("Mercredi");
			document.getElementById("boutonJourLundi").style.background= "#75A3E7 ";
			document.getElementById("boutonJourMardi").style.background= "#75A3E7 ";
			document.getElementById("boutonJourMercredi").style.background= "#4C6CA0 ";
			document.getElementById("boutonJourJeudi").style.background= "#75A3E7 ";
			document.getElementById("boutonJourVendredi").style.background= "#75A3E7 ";
			break;
		case 3:
			jourSemaine = "Jeu";
			fetch_ended(3, true);
			//alert("Jeudi");
			document.getElementById("boutonJourLundi").style.background= "#75A3E7 ";
	    	document.getElementById("boutonJourMardi").style.background= "#75A3E7 ";
			document.getElementById("boutonJourMercredi").style.background= "#75A3E7 ";
			document.getElementById("boutonJourJeudi").style.background= "#4C6CA0 ";
			document.getElementById("boutonJourVendredi").style.background= "#75A3E7 ";
			break;
		case 4:
			jourSemaine = "Ven";
			fetch_ended(4, true);
			//alert("Vendredi");
			document.getElementById("boutonJourLundi").style.background= "#75A3E7 ";
			document.getElementById("boutonJourMardi").style.background= "#75A3E7 ";
			document.getElementById("boutonJourMercredi").style.background= "#75A3E7 ";
			document.getElementById("boutonJourJeudi").style.background= "#75A3E7 ";
			document.getElementById("boutonJourVendredi").style.background= "#4C6CA0 ";
			break;
		default:
			alert('Error');
			break;
	}
	afficherDate(day);
	//document.getElementsByClassName('gridsckd')[0].innerHTML = jourSemaine;
}
function afficherDate(day = 0){
	document.getElementById("date-number").innerHTML = (getDateOfWeek(weakYear.semaine,weakYear.an)[0] + day) + "/" + getDateOfWeek(weakYear.semaine,weakYear.an)[1];
}
function getDateOfWeek(weekNumber,year){
	var dateDay = new Date(year, 0, 1+((weekNumber-1)*7));
	var tabDateDay = [dateDay.getDate(), dateDay.getMonth() + 1];
    return tabDateDay;
}

function setEnable(){

	if(document.getElementById("boutonOuvrir").style.visibility == "visible"){
		document.getElementById("volet").style.visibility = "visible";
		window.location.href="#volet"
		document.getElementById("boutonOuvrir").style.visibility = "hidden";
		document.getElementById("boutonFermer").style.visibility = "visible";
		document.getElementById("boutonJourLundi").disabled = true;
		document.getElementById("boutonJourMardi").disabled = true;
		document.getElementById("boutonJourMercredi").disabled = true;
		document.getElementById("boutonJourJeudi").disabled = true;
		document.getElementById("boutonJourVendredi").disabled = true;



	}else{
		document.getElementById("volet").style.visibility = "hidden";
		window.location.href="#volet_clos"
		document.getElementById("boutonOuvrir").style.visibility = "visible";
		document.getElementById("boutonFermer").style.visibility = "hidden";
		document.getElementById("boutonJourLundi").disabled = false;
		document.getElementById("boutonJourMardi").disabled = false;
		document.getElementById("boutonJourMercredi").disabled = false;
		document.getElementById("boutonJourJeudi").disabled = false;
		document.getElementById("boutonJourVendredi").disabled = false;
	}

}

function setFiltresVisibles(numBoutons){
	switch (numBoutons) {
		case 0:
			if (document.getElementById("tableProf").style.display == "none"){
				document.getElementById("tableProf").style.display = "block";
				document.getElementById("div-sal").style.display = "none";
				document.getElementById("div-mod").style.display = "none";
    		}else{
				document.getElementById("tableProf").style.display = "none";
			}
			break;
		case 1:
			if (document.getElementById("div-sal").style.display == "none"){
				document.getElementById("div-sal").style.display = "block";
				document.getElementById("tableProf").style.display = "none";
				document.getElementById("div-mod").style.display = "none";
			}else{
				document.getElementById("div-sal").style.display = "none";
			}
			break;
		case 2:
			if (document.getElementById("div-mod").style.display == "none"){
				document.getElementById("div-mod").style.display = "block";
				document.getElementById("div-sal").style.display = "none";
				document.getElementById("tableProf").style.display = "none";
			}else{
				document.getElementById("div-mod").style.display = "none";
			}
			break;
		default:
		    document.getElementById("div-mod").style.display = "none";
			document.getElementById("div-sal").style.display = "none";
			document.getElementById("tableProf").style.display = "none";
	}

}


function setEnableContact(){

	var tamere = document.getElementById('menu-edt');
	var parent = document.body;
	parent.removeChild(tamere);
}
